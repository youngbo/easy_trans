package com.fhs.core.trans.convert;

public interface Convert {
    <T> T convert(Object o, Class<T> targetType) throws ClassCastException;
}
