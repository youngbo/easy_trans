package com.fhs.trans.config;

import com.fhs.core.trans.util.ReflectUtils;
import com.fhs.trans.extend.MybatisFlexSimpleTransDiver;
import com.fhs.trans.extend.MybatisFlexTransableRegister;
import com.mybatisflex.annotation.Id;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * Mybatis-Flex 适配器。
 *
 * @author wanglei
 * @author wangshuai
 */
@Slf4j
@Configuration
public class EasyTransMybatisFlexConfig {

    /**
     * Service 的包路径。
     */
    @Value("${easy-trans.autotrans.package:com.*.*.service.impl}")
    private String packageNames;

    @Bean
    @ConditionalOnProperty(name = "easy-trans.is-enable-auto", havingValue = "true")
    public MybatisFlexTransableRegister mybatisFlexTransableRegister() {
        MybatisFlexTransableRegister result = new MybatisFlexTransableRegister();
        result.setPackageNames(packageNames);
        return result;
    }

    @Bean
    @Primary
    public MybatisFlexSimpleTransDiver mybatisFlexSimpleTransDiver() {
        ReflectUtils.ID_ANNO.add(Id.class);
        return new MybatisFlexSimpleTransDiver();
    }

}
