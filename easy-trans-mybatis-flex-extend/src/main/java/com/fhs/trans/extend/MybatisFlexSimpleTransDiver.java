package com.fhs.trans.extend;

import com.fhs.common.utils.StringUtil;
import com.fhs.core.trans.vo.VO;
import com.fhs.trans.service.impl.SimpleTransService;
import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.core.mybatis.Mappers;
import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.table.IdInfo;
import com.mybatisflex.core.table.TableInfo;
import com.mybatisflex.core.table.TableInfoFactory;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Mybatis-Flex 简单翻译驱动。
 *
 * @author wanglei
 * @author wangshuai
 */
@Slf4j
@SuppressWarnings({"rawtypes", "unchecked"})
public class MybatisFlexSimpleTransDiver implements SimpleTransService.SimpleTransDiver {

    @Override
    public List<? extends VO> findByIds(List<? extends Serializable> ids, Class<? extends VO> targetClass, String uniqueField) {
        return findByIds(ids, targetClass, uniqueField, null);
    }

    @Override
    public List<? extends VO> findByIds(List<? extends Serializable> ids, Class<? extends VO> targetClass, String uniqueField, Set<String> targetFields) {
        TableInfo tableInfo = TableInfoFactory.ofEntityClass(targetClass);
        uniqueField = this.getUniqueField(tableInfo, uniqueField);
        QueryWrapper queryWrapper = genWrapper(tableInfo, targetFields, uniqueField);
        queryWrapper.where(getQueryColumn(tableInfo, uniqueField).in(ids));
        return getMapper(targetClass).selectListByQuery(queryWrapper);
    }

    @Override
    public VO findById(Serializable id, Class<? extends VO> targetClass, String uniqueField) {
        return findById(id, targetClass, uniqueField, null);
    }

    @Override
    public VO findById(Serializable id, Class<? extends VO> targetClass, String uniqueField, Set<String> targetFields) {
        TableInfo tableInfo = TableInfoFactory.ofEntityClass(targetClass);
        uniqueField = this.getUniqueField(tableInfo, uniqueField);
        QueryWrapper queryWrapper = genWrapper(tableInfo, targetFields, uniqueField);
        queryWrapper.where(getQueryColumn(tableInfo, uniqueField).eq(id));
        return (VO) getMapper(targetClass).selectOneById(id);
    }

    /**
     * 生成查询 {@link QueryWrapper} 对象。
     *
     * @param tableInfo    表信息
     * @param targetFields 目标字段
     * @param uniqueField  唯一键
     * @return {@link QueryWrapper}
     */
    private QueryWrapper genWrapper(TableInfo tableInfo, Set<String> targetFields, String uniqueField) {
        QueryWrapper queryWrapper = QueryWrapper.create();
        if (targetFields != null && !targetFields.isEmpty()) {
            targetFields.add(getKeyProperty(tableInfo));
            if (!StringUtil.isEmpty(uniqueField)) {
                targetFields.add(uniqueField);
            }
            queryWrapper.select(targetFields.stream()
                    .map(tableInfo::getColumnByProperty)
                    .map(tableInfo::buildQueryColumn)
                    .toArray(QueryColumn[]::new));
        }
        return queryWrapper;
    }

    /**
     * 如果配置了 {@code uniqueField} 则返回 {@code uniqueField} 没有就返回主键。
     *
     * @param tableInfo   表信息
     * @param uniqueField 唯一键
     * @return 唯一键
     */
    private String getUniqueField(TableInfo tableInfo, String uniqueField) {
        if (!StringUtil.isEmpty(uniqueField)) {
            return uniqueField;
        }
        return getKeyProperty(tableInfo);
    }

    /**
     * 获取主键属性。
     *
     * @param tableInfo 表信息
     * @return 主键属性
     */
    private String getKeyProperty(TableInfo tableInfo) {
        List<IdInfo> primaryKeyList = tableInfo.getPrimaryKeyList();
        // 类中没有使用 @Id 注解标记主键
        if (primaryKeyList.isEmpty()) {
            throw new IllegalArgumentException(String.format("类 %s 中没有找到 @Id 定义的主键，请使用 uniqueField 指定主键。", tableInfo.getEntityClass().getName()));
        }
        // MyBatis-Flex 支持多主键，这里只返回按照定义顺序的第一个主键。
        return primaryKeyList.get(0).getProperty();
    }

    /**
     * 获取查询列。
     *
     * @param tableInfo 表信息
     * @param field     类属性
     * @return {@link QueryColumn}
     */
    private QueryColumn getQueryColumn(TableInfo tableInfo, String field) {
        String column = tableInfo.getColumnByProperty(field);
        return tableInfo.buildQueryColumn(column);
    }

    /**
     * 根据实体类获取 {@link BaseMapper} 对象。
     *
     * @param entity 实体类
     * @return {@link BaseMapper}
     */
    public BaseMapper getMapper(Class entity) {
        return Mappers.ofEntityClass(entity);
    }

}
