package com.fhs.trans.extend;

import com.fhs.core.trans.vo.VO;
import com.fhs.trans.service.AutoTransable;
import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.core.mybatis.Mappers;
import com.mybatisflex.core.query.QueryWrapper;

import java.io.Serializable;
import java.util.List;

/**
 * Mybatis-Flex 自动翻译适配器。
 *
 * @author wangshuai
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class MybatisFlexTransableAdapter implements AutoTransable {

    private final BaseMapper<? extends VO> baseMapper;

    public MybatisFlexTransableAdapter(Class<? extends VO> voClass) {
        this.baseMapper = Mappers.ofEntityClass(voClass);
    }

    @Override
    public List findByIds(List ids) {
        return baseMapper.selectListByIds(ids);
    }

    @Override
    public List selectByIds(List ids) {
        return baseMapper.selectListByIds(ids);
    }

    @Override
    public List select() {
        return baseMapper.selectListByQuery(QueryWrapper.create());
    }

    @Override
    public VO selectById(Object primaryValue) {
        return baseMapper.selectOneById((Serializable) primaryValue);
    }

}
